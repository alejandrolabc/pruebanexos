/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.msPersona.restapi;

import com.nexos.msPersona.dto.PersonaDto;
import com.nexos.msPersona.response.ResourceResponse;
import com.nexos.msPersona.services.AuditServiciosService;
import com.nexos.msPersona.services.PersonaService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.msPersona.entities.AuditServicios;
import com.nexos.msPersona.entities.Persona;

/**
 *
 * @author luis_balaguera
 */
@RestController
public class PersonaController {

	/**
	 * variable de log4j para logs de la aplicación
	 */
	private static final Logger logger = LogManager.getLogger(PersonaController.class);

	@Autowired
	private PersonaService personaService;

	@Autowired
	private AuditServiciosService auditServiciosService;

	/**
	 * guardarAuditoria.
	 *
	 * @param path este parametro contiene la ruta del path que consultara el
	 *             servicio
	 * @return del archivo JSONObject que responde el servicio restful
	 */
	public void guardarAuditoria(String path, String metodo, String codigo, String descripcion) {
		try {
			AuditServicios auditoria = new AuditServicios();
			auditoria.setServicio("msPersona");
			auditoria.setPath(path);
			auditoria.setMetodo(metodo);
			auditoria.setCodigo(codigo);
			auditoria.setDescripcion(descripcion);
			auditoria.setFecha(new Date());
			auditServiciosService.save(auditoria);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error al guardar auditoria");
		}
	}

	@RequestMapping(value = "/personas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse getPersonas() {
		guardarAuditoria("/personas","GET","0","entro al servicio");
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			List<Persona> personas = personaService.findAll();
			List<PersonaDto> personasDto = new ArrayList<PersonaDto>();
			personas.parallelStream().forEach(p -> {
				PersonaDto personaDto = new PersonaDto(p.getId(), p.getNombre(), p.getApellido(), p.getDocumento(),
						p.getCelular());
				personasDto.add(personaDto);
			});
			resourceResponse.setPersonas(personasDto);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/personas","GET",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/personas","GET",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/personas/{idPersona}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse getPersonaById(@PathVariable("idPersona") Integer idPersona) {
		guardarAuditoria("/personas/"+idPersona,"GET","0","entro al servicio");
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			Persona p = personaService.findById(idPersona);
			List<PersonaDto> personasDto = new ArrayList<PersonaDto>();
			personasDto
					.add(new PersonaDto(p.getId(), p.getNombre(), p.getApellido(), p.getDocumento(), p.getCelular()));
			resourceResponse.setPersonas(personasDto);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/personas/"+idPersona,"GET",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/personas/"+idPersona,"GET",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/personas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse createPersonas(@RequestBody Persona persona) {
		guardarAuditoria("/personas","POST","0",persona.toString());
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			Persona personaNew = personaService.save(persona);
			List<PersonaDto> personasDto = new ArrayList<PersonaDto>();
			personasDto.add(new PersonaDto(personaNew.getId(), personaNew.getNombre(), personaNew.getApellido(),
					personaNew.getDocumento(), personaNew.getCelular()));
			resourceResponse.setPersonas(personasDto);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/personas","POST",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/personas","POST",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/personas/{idPersona}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse updatePersonas(@RequestBody Persona persona, @PathVariable("idPersona") Integer idPersona) {
		guardarAuditoria("/personas/"+idPersona,"PUT","0",persona.toString());
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			Persona personaUpdate = personaService.findById(idPersona);
			personaUpdate.setNombre(persona.getNombre());
			personaUpdate.setApellido(persona.getApellido());
			personaUpdate.setDocumento(persona.getDocumento());
			personaUpdate.setCelular(persona.getCelular());
			Persona personaNew = personaService.save(personaUpdate);
			List<PersonaDto> personasDto = new ArrayList<PersonaDto>();
			personasDto.add(new PersonaDto(personaNew.getId(), personaNew.getNombre(), personaNew.getApellido(),
					personaNew.getDocumento(), personaNew.getCelular()));
			resourceResponse.setPersonas(personasDto);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/personas/"+idPersona,"PUT",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/personas/"+idPersona,"PUT",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/personas/{idPersona}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse deletePersonas(@PathVariable("idPersona") Integer idPersona) {
		guardarAuditoria("/personas/"+idPersona,"DELETE","0","entro al servicio");
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			Persona personaDelete = personaService.findById(idPersona);
			personaService.delete(personaDelete);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/personas/"+idPersona,"DELETE",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/personas/"+idPersona,"DELETE",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

}
