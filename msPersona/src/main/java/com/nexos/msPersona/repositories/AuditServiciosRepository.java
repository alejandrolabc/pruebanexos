package com.nexos.msPersona.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.msPersona.entities.AuditServicios;

/**
*
* @author luis_balaguera
*/
public interface AuditServiciosRepository extends JpaRepository<AuditServicios, Integer>{

}
