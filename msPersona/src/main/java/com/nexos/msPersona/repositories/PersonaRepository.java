package com.nexos.msPersona.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.msPersona.entities.Persona;

/**
*
* @author luis_balaguera
*/
public interface PersonaRepository extends JpaRepository<Persona, Integer>{

}
