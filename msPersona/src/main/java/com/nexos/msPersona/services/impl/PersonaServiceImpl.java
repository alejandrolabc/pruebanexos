package com.nexos.msPersona.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexos.msPersona.entities.Persona;
import com.nexos.msPersona.repositories.PersonaRepository;
import com.nexos.msPersona.services.PersonaService;

/**
*
* @author luis_balaguera
*/
@Component
public class PersonaServiceImpl implements PersonaService{
	
	@Autowired
	private PersonaRepository personaRepository;
	
	@Override
	public List<Persona> findAll() {

		/**
		 * consulta el listado de todas las personas
		 */
		List<Persona> personasList = (List<Persona>) personaRepository.findAll();

		return personasList;
	}
	
	@Override
	public Persona findById(Integer idPersona) {

		/**
		 * consulta las personas por id
		 */
		Persona persona = personaRepository.getOne(idPersona);

		return persona;
	}
	
	@Override
	@Transactional
	public Persona save(Persona persona) {
		Persona personaNew = personaRepository.save(persona);
		return personaNew;
	}
	
	@Override
	public void delete(Persona persona) {
		personaRepository.delete(persona);
	}

}
