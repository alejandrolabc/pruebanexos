/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.msPersona.services;

import java.util.List;

import com.nexos.msPersona.entities.AuditServicios;

/**
 *
 * @author luis_balaguera
 */
public interface AuditServiciosService {
	
	/**
	 * Guarda una auditoria en DB
	 * 
	 * @param persona
	 */
	public AuditServicios save(AuditServicios auditoria);
	
}
