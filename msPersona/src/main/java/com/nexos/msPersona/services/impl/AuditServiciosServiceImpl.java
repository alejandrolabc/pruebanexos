package com.nexos.msPersona.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexos.msPersona.entities.AuditServicios;
import com.nexos.msPersona.repositories.AuditServiciosRepository;
import com.nexos.msPersona.services.AuditServiciosService;

/**
*
* @author luis_balaguera
*/
@Component
public class AuditServiciosServiceImpl implements AuditServiciosService{
	
	@Autowired
	private AuditServiciosRepository auditServiciosRepository;
	
	@Override
	@Transactional
	public AuditServicios save(AuditServicios auditoria) {
		return auditServiciosRepository.save(auditoria);
	}
	

}
