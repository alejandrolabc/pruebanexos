/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.msPersona.services;

import java.util.List;

import com.nexos.msPersona.entities.Persona;

/**
 *
 * @author luis_balaguera
 */
public interface PersonaService {
	
	/**
	 * Consulta todo el listado de Personas
	 * 
	 * @return
	 */
	public List<Persona> findAll();
	
	/**
	 * Consulta una Persona por id
	 * 
	 * @param id
	 * @return
	 */
	public Persona findById(Integer id);
	
	/**
	 * Guarda una Persona en DB
	 * 
	 * @param persona
	 */
	public Persona save(Persona persona);
	
	/**
	 * Elimina una Persona en DB
	 * 
	 * @param persona
	 */
	public void delete(Persona persona);
}
