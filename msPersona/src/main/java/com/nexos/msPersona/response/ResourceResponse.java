package com.nexos.msPersona.response;

import java.util.List;

import com.nexos.msPersona.dto.PersonaDto;

/**
*
* @author luis_balaguera
*/
public class ResourceResponse extends Response{
		
	private List<PersonaDto> personas;
	
	
	public ResourceResponse(Integer code, String message, String resource, List<PersonaDto> personas) {
		super(code, message, resource);
		this.personas = personas;
	}


	public List<PersonaDto> getPersonas() {
		return personas;
	}

	public void setPersonas(List<PersonaDto> personas) {
		this.personas = personas;
	}
	
	
}
