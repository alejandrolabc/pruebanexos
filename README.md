PruebaMicroServiciosNexos
================

Autor
---------

*  **Luis Alejandro Balaguera**



Versión
--------
1.0

Descripción
--------------
La finalidad de este proyecto, es la de generar una serie de microservicios para cumplir con la prueba propuesta por Nexos. 

Ambiente
-----------

A continuación, se describe el ambiente en que se diseñó esta versión del proyecto;
<table>
<tr><th>Descripción</th><th>Nombre</th><th>Versión</th></tr>
<tr><td>Sistema Operativo</td><td>Windows</td><td>10 x64</td></tr>
<tr><td>Gestor de dependencias</td><td>Maven</td><td>3.6.3</td></tr>
<tr><td>Framework</td><td>SpringBoot</td><td>2.3.1.BUILD-SNAPSHOT</td></tr>
<tr><td>IDE</td><td>Eclipse</td><td>2020-03 (4.15.0)</td></tr>
<tr><td>Conector Base de Datos</td><td>MySQL</td><td>8.0.16</td></tr>
<tr><td>JDK</td><td>Java</td><td>1.8 x64</td></tr>
</table>
 
# Procesos

## Instalación
El proyecto se encuentra en un repositorio GitLab, para poderlo descargar se deben seguir los siguientes pasos:

 1. Tener instalado un cliente GIT 
 2. Descargar el repositorio al local con la URL proporcionada
 3. Ubicar el branch master
 4. Abrir el proyecto en el IDE de preferencia

## Configuracion
Para que los microservicios funcionen se debe crear primeramente la base de datos, esta ya se encuentra en el repositorio con el nombre de familia_nexos.sql; así mismo debe llamarse la base de datos para que no se tenga problemas de ejecución.

## Compilación
Una vez se descargue el proyecto, se deberan tener 3 jars los cuales correspondean a los microservicios, que a su vez tienen enbebido un servidor tomcat que proporciona springboot para su funcionamiento por separado y compilar cada uno de los jars por separados, no importa el orden.

## Despliegue
Para poder realizar el despliegue de los microservicios se debe hacer lo siguiente
```
 1. Compilar el proyecto dando click derecho al proyecto  > run as > Maven Install
 2. En caso de que salga error probar dando click dereco al proyecto > maven > Update Project. Marcar la casilla Force Update Snapshots/Releases y luego OK. Volver a realizar el paso 1
 2. Ubicar la opcion run configurations del menu Run que se encuentra en la parte superior
 3. Crear nuevo launch configuration
 4. Buscar la clase principal del proyecto y correr.
```
Estos pasos estan realizados con el IDE Eclipse
