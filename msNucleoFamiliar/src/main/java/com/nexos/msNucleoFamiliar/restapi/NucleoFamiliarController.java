package com.nexos.msNucleoFamiliar.restapi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.msNucleoFamiliar.restapi.NucleoFamiliarController;
import com.nexos.msNucleoFamiliar.services.NucleoFamiliarService;
import com.nexos.msNucleoFamiliar.services.ParentescoService;
import com.nexos.msNucleoFamiliar.services.PersonaService;
import com.nexos.msNucleoFamiliar.entities.AuditServicios;
import com.nexos.msNucleoFamiliar.services.AuditServiciosService;
import com.nexos.msNucleoFamiliar.response.ResourceResponse;
import com.nexos.msNucleoFamiliar.dto.NucleoFamiliarDto;
import com.nexos.msNucleoFamiliar.entities.NucleoFamiliar;
import com.nexos.msNucleoFamiliar.entities.Parentesco;
import com.nexos.msNucleoFamiliar.entities.Persona;

/**
 *
 * @author luis_balaguera
 */
@RestController
public class NucleoFamiliarController {

	/**
	 * variable de log4j para logs de la aplicación
	 */
	private static final Logger logger = LogManager.getLogger(NucleoFamiliarController.class);

	@Autowired
	NucleoFamiliarService nucleoFamiliarService;

	@Autowired
	ParentescoService parentescoService;

	@Autowired
	PersonaService personaService;
	
	@Autowired
	private AuditServiciosService auditServiciosService;
	
	/**
	 * guardarAuditoria.
	 *
	 * @param path este parametro contiene la ruta del path que consultara el
	 *             servicio
	 * @return del archivo JSONObject que responde el servicio restful
	 */
	public void guardarAuditoria(String path, String metodo, String codigo, String descripcion) {
		try {
			AuditServicios auditoria = new AuditServicios();
			auditoria.setServicio("msNucleoFamiliar");
			auditoria.setPath(path);
			auditoria.setMetodo(metodo);
			auditoria.setCodigo(codigo);
			auditoria.setDescripcion(descripcion);
			auditoria.setFecha(new Date());
			auditServiciosService.save(auditoria);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error al guardar auditoria");
		}
	}

	@RequestMapping(value = "/nucleoFamiliar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse getNucleoFamiliar() {
		guardarAuditoria("/nucleoFamiliar","GET","0","entro al servicio");
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			List<NucleoFamiliar> nucleoFamiliar = nucleoFamiliarService.findAll();
			List<NucleoFamiliarDto> nucleoFamiliarDto = new ArrayList<NucleoFamiliarDto>();
			nucleoFamiliar.parallelStream().forEach(n -> {
				NucleoFamiliarDto nfDto = new NucleoFamiliarDto(n.getId(), n.getNombre(), n.getApellido(),
						n.getDocumento(), n.getContacto(), n.getParentesco().getTipo());
				nucleoFamiliarDto.add(nfDto);
			});
			resourceResponse.setNucleoFamiliar(nucleoFamiliarDto);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/nucleoFamiliar","GET",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/nucleoFamiliar","GET",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/persona/{idPersona}/nucleoFamiliar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse getNucleoFamiliarByPersona(@PathVariable("idPersona") Integer idPersona) {
		guardarAuditoria("/persona/"+idPersona+"/nucleoFamiliar","GET","0","entro al servicio");
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			List<NucleoFamiliar> nucleoFamiliar = nucleoFamiliarService.findByPersona(idPersona);
			List<NucleoFamiliarDto> nucleoFamiliarDto = new ArrayList<NucleoFamiliarDto>();
			nucleoFamiliar.parallelStream().forEach(n -> {
				NucleoFamiliarDto nfDto = new NucleoFamiliarDto(n.getId(), n.getNombre(), n.getApellido(),
						n.getDocumento(), n.getContacto(), n.getParentesco().getTipo());
				nucleoFamiliarDto.add(nfDto);
			});
			resourceResponse.setNucleoFamiliar(nucleoFamiliarDto);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/persona/"+idPersona+"/nucleoFamiliar","GET",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/persona/"+idPersona+"/nucleoFamiliar","GET",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/nucleoFamiliar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse createNucleoFamiliar(@RequestBody NucleoFamiliar nucleoFamiliar) {
		guardarAuditoria("/nucleoFamiliar","POST","0",nucleoFamiliar.toString());
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			Persona persona = personaService.findById(nucleoFamiliar.getPersona().getId());
			nucleoFamiliar.setPersona(persona);
			Parentesco parentesco = parentescoService.findById(nucleoFamiliar.getParentesco().getId());
			nucleoFamiliar.setParentesco(parentesco);
			NucleoFamiliar nucleoFamiliarNew = nucleoFamiliarService.save(nucleoFamiliar);
			List<NucleoFamiliarDto> nucleoFamiliarDto = new ArrayList<NucleoFamiliarDto>();
			nucleoFamiliarDto.add(new NucleoFamiliarDto(nucleoFamiliarNew.getId(), nucleoFamiliarNew.getNombre(),
					nucleoFamiliarNew.getApellido(), nucleoFamiliarNew.getDocumento(), nucleoFamiliarNew.getContacto(),
					nucleoFamiliarNew.getParentesco().getTipo()));
			resourceResponse.setNucleoFamiliar(nucleoFamiliarDto);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/nucleoFamiliar","POST",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/nucleoFamiliar","POST",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/nucleoFamiliar/{idNucleoFamiliar}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse updateNucleoFamiliar(@RequestBody NucleoFamiliar nucleoFamiliar,
			@PathVariable("idNucleoFamiliar") Integer idNucleoFamiliar) {
		guardarAuditoria("/nucleoFamiliar/"+idNucleoFamiliar,"PUT","0",nucleoFamiliar.toString());
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			NucleoFamiliar nucleoFamiliarUpdate = nucleoFamiliarService.findById(idNucleoFamiliar);
			nucleoFamiliarUpdate.setNombre(nucleoFamiliar.getNombre());
			nucleoFamiliarUpdate.setApellido(nucleoFamiliar.getApellido());
			nucleoFamiliarUpdate.setDocumento(nucleoFamiliar.getDocumento());
			nucleoFamiliarUpdate.setContacto(nucleoFamiliar.getContacto());
			Parentesco parentesco = parentescoService.findById(nucleoFamiliar.getParentesco().getId());
			nucleoFamiliarUpdate.setParentesco(parentesco);
			NucleoFamiliar nucleoFamiliarNew = nucleoFamiliarService.save(nucleoFamiliarUpdate);
			List<NucleoFamiliarDto> nucleoFamiliarDto = new ArrayList<NucleoFamiliarDto>();
			nucleoFamiliarDto.add(new NucleoFamiliarDto(nucleoFamiliarNew.getId(), nucleoFamiliarNew.getNombre(),
					nucleoFamiliarNew.getApellido(), nucleoFamiliarNew.getDocumento(), nucleoFamiliarNew.getContacto(),
					nucleoFamiliarNew.getParentesco().getTipo()));
			resourceResponse.setNucleoFamiliar(nucleoFamiliarDto);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/nucleoFamiliar/"+idNucleoFamiliar,"PUT",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/nucleoFamiliar/"+idNucleoFamiliar,"PUT",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/nucleoFamiliar/{idNucleoFamiliar}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResourceResponse deleteNucleoFamiliar(@PathVariable("idNucleoFamiliar") Integer idNucleoFamiliar) {
		guardarAuditoria("/nucleoFamiliar/"+idNucleoFamiliar,"DELETE","0","entro al servicio");
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			NucleoFamiliar nucleoFamiliarDelete = nucleoFamiliarService.findById(idNucleoFamiliar);
			nucleoFamiliarService.delete(nucleoFamiliarDelete);
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
			resourceResponse.setMessage("OK");
			logger.info("Correcto");
			guardarAuditoria("/nucleoFamiliar/"+idNucleoFamiliar,"DELETE",resourceResponse.getCode().toString(),"OK");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
			guardarAuditoria("/nucleoFamiliar/"+idNucleoFamiliar,"DELETE",resourceResponse.getCode().toString(),e.getMessage());
		}
		return resourceResponse;
	}

}
