package com.nexos.msNucleoFamiliar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsNucleoFamiliarApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsNucleoFamiliarApplication.class, args);
	}

}
