package com.nexos.msNucleoFamiliar.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Clase Nucleo Familiar
 * 
 * @author luis_Balaguera
 *
 */
@Entity
@Table(name = "nucleo_familiar")
@ApiModel("Model nucleo familiar")
@ToString
public class NucleoFamiliar implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "Id único del nucleo familiar", required = true)
	private Integer id;
	
	@NotNull
	@ApiModelProperty(value = "nombre del familiar", required = true)
	@Column(name = "nombre")
	private String nombre;
	
	@NotNull
	@ApiModelProperty(value = "apellido del familiar", required = true)
	@Column(name = "apellido")
	private String apellido;
	
	@NotNull
	@ApiModelProperty(value = "documento de identificación del familiar", required = true)
	@Column(name = "documento")
	private String documento;
	
	@NotNull
	@ApiModelProperty(value = "número de contacto del familiar", required = true)
	@Column(name = "contacto")
	private String contacto;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_persona")
	@ApiModelProperty(value = "persona", required = true)
	private Persona persona;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_parentesco")
	@ApiModelProperty(value = "parentesco", required = true)
	private Parentesco parentesco;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Parentesco getParentesco() {
		return parentesco;
	}

	public void setParentesco(Parentesco parentesco) {
		this.parentesco = parentesco;
	}
	
}
