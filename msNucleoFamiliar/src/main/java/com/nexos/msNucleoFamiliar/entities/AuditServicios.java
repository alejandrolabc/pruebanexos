package com.nexos.msNucleoFamiliar.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Clase AuditServicios
 * 
 * @author luis_Balaguera
 *
 */
@Entity
@Table(name = "audit_servicios")
@ApiModel("Model Auditoria de servicios")
@ToString
public class AuditServicios implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "Id único de la auditoria", required = true)
	private Integer id;
	
	@NotNull
	@ApiModelProperty(value = "nombre del servicio", required = true)
	@Column(name = "servicio")
	private String servicio;
	
	@NotNull
	@ApiModelProperty(value = "path del servicio", required = true)
	@Column(name = "path")
	private String path;
	
	@NotNull
	@ApiModelProperty(value = "metodo del servicio", required = true)
	@Column(name = "metodo")
	private String metodo;
	
	@NotNull
	@ApiModelProperty(value = "codigo de respuesta del servicio", required = true)
	@Column(name = "codigo")
	private String codigo;
	
	@NotNull
	@ApiModelProperty(value = "descripcion de respuesta del servicio", required = true)
	@Column(name = "descripcion")
	private String descripcion;
	
	@NotNull
	@ApiModelProperty(value = "fecha de la auditoria", required = true)
	@Column(name = "fecha")
	private Date fecha;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
		
}
