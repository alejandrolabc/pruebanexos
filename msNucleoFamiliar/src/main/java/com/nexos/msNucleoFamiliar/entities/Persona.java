package com.nexos.msNucleoFamiliar.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Clase Persona
 * 
 * @author luis_Balaguera
 *
 */
@Entity
@Table(name = "persona")
@ApiModel("Model Persona")
@ToString
public class Persona implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "Id único de la persona", required = true)
	private Integer id;
	
	@NotNull
	@ApiModelProperty(value = "nombre de la persona", required = true)
	@Column(name = "nombre")
	private String nombre;
	
	@NotNull
	@ApiModelProperty(value = "apellido de la persona", required = true)
	@Column(name = "apellido")
	private String apellido;
	
	@NotNull
	@ApiModelProperty(value = "documento de identificación", required = true)
	@Column(name = "documento")
	private String documento;
	
	@NotNull
	@ApiModelProperty(value = "número de contacto", required = true)
	@Column(name = "celular")
	private String celular;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}
	
}
