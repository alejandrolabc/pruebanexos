package com.nexos.msNucleoFamiliar.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.msNucleoFamiliar.entities.Persona;

/**
*
* @author luis_balaguera
*/
public interface PersonaRepository extends JpaRepository<Persona, Integer>{

}
