package com.nexos.msNucleoFamiliar.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.msNucleoFamiliar.entities.AuditServicios;

/**
*
* @author luis_balaguera
*/
public interface AuditServiciosRepository extends JpaRepository<AuditServicios, Integer>{

}
