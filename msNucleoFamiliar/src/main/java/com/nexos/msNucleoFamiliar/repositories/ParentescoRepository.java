package com.nexos.msNucleoFamiliar.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.msNucleoFamiliar.entities.Parentesco;

/**
*
* @author luis_balaguera
*/
public interface ParentescoRepository extends JpaRepository<Parentesco, Integer>{

}
