package com.nexos.msNucleoFamiliar.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nexos.msNucleoFamiliar.entities.NucleoFamiliar;

/**
*
* @author luis_balaguera
*/
public interface NucleoFamiliarRepository extends JpaRepository<NucleoFamiliar, Integer>{
	
	@Query(value = "SELECT n FROM NucleoFamiliar n WHERE n.persona.id = :idPersona")
	public List<NucleoFamiliar> findByPersona(@Param("idPersona") Integer idPersona);

}
