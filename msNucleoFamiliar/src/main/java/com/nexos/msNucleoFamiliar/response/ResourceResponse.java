package com.nexos.msNucleoFamiliar.response;

import java.util.List;

import com.nexos.msNucleoFamiliar.dto.NucleoFamiliarDto;
import com.nexos.msNucleoFamiliar.response.Response;

/**
*
* @author luis_balaguera
*/
public class ResourceResponse extends Response {
	
private List<NucleoFamiliarDto> nucleoFamiliar;
	
	
	public ResourceResponse(Integer code, String message, String resource, List<NucleoFamiliarDto> nucleoFamiliar) {
		super(code, message, resource);
		this.nucleoFamiliar = nucleoFamiliar;
	}


	public List<NucleoFamiliarDto> getNucleoFamiliar() {
		return nucleoFamiliar;
	}

	public void setNucleoFamiliar(List<NucleoFamiliarDto> nucleoFamiliar) {
		this.nucleoFamiliar = nucleoFamiliar;
	}

}
