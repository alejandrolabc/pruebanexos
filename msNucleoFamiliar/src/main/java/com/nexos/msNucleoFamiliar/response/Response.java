package com.nexos.msNucleoFamiliar.response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

/**
 * Estructura general de respuesta que se enviará al cliente
 * 
 * @author luis_balaguera
 *
 */
@ToString 
@Api(tags = "Respuesta genérica compartida en todo el API")
public class Response {
	
	@ApiModelProperty(value="Código de la respuesta del servicio, típicamente un código HTTP status", required=false)
	private Integer code;
	@ApiModelProperty(value="Mensaje asociado a la respuesta de un servicio", required=false)
	private String message;
	@ApiModelProperty(value="Carga o payload adicional que pueda tener esta respuesta", required=false)
	private String resource;
	
	public Response(Integer code, String message, String resource) {
		super();
		this.code = code;
		this.message = message;
		this.resource = resource;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}

}
