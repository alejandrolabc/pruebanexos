package com.nexos.msNucleoFamiliar.dto;

/**
*
* @author luis_balaguera
*/
public class NucleoFamiliarDto {
	
	private Integer id;
    
    private String nombre;
    
    private String apellido;
    
    private String documento;
    
    private String contacto;
    
    private String parentesco;

	public NucleoFamiliarDto(Integer id, String nombre, String apellido, String documento, String contacto,
			String parentesco) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.documento = documento;
		this.contacto = contacto;
		this.parentesco = parentesco;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getParentesco() {
		return parentesco;
	}

	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}

}
