package com.nexos.msNucleoFamiliar.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexos.msNucleoFamiliar.entities.Parentesco;
import com.nexos.msNucleoFamiliar.repositories.ParentescoRepository;
import com.nexos.msNucleoFamiliar.services.ParentescoService;

/**
*
* @author luis_balaguera
*/
@Component
public class ParentescoServiceImpl implements ParentescoService{
	
	@Autowired
	ParentescoRepository parentescoRepository;
	
	@Override
	public Parentesco findById(Integer idParentesco) {

		/**
		 * consulta los parentescos por id
		 */
		Parentesco parentesco = (Parentesco) parentescoRepository.getOne(idParentesco);

		return parentesco;
	}
}
