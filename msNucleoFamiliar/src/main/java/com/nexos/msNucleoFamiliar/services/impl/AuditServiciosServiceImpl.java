package com.nexos.msNucleoFamiliar.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexos.msNucleoFamiliar.entities.AuditServicios;
import com.nexos.msNucleoFamiliar.repositories.AuditServiciosRepository;
import com.nexos.msNucleoFamiliar.services.AuditServiciosService;

/**
*
* @author luis_balaguera
*/
@Component
public class AuditServiciosServiceImpl implements AuditServiciosService{
	
	@Autowired
	private AuditServiciosRepository auditServiciosRepository;
	
	@Override
	@Transactional
	public AuditServicios save(AuditServicios auditoria) {
		return auditServiciosRepository.save(auditoria);
	}
	

}
