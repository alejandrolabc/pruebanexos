package com.nexos.msNucleoFamiliar.services;

import com.nexos.msNucleoFamiliar.entities.Parentesco;

public interface ParentescoService {
	
	/**
	 * Consulta un parentesco por id
	 * 
	 * @param id
	 * @return
	 */
	public Parentesco findById(Integer id);

}
