package com.nexos.msNucleoFamiliar.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexos.msNucleoFamiliar.repositories.NucleoFamiliarRepository;
import com.nexos.msNucleoFamiliar.services.NucleoFamiliarService;
import com.nexos.msNucleoFamiliar.entities.NucleoFamiliar;

/**
*
* @author luis_balaguera
*/
@Component
public class NucleoFamiliarServiceImpl implements NucleoFamiliarService{
	
	@Autowired
	NucleoFamiliarRepository nucleoFamiliarRepository;
	
	@Override
	public List<NucleoFamiliar> findAll() {

		/**
		 * consulta el listado de todos los familiares
		 */
		List<NucleoFamiliar> familiaList = (List<NucleoFamiliar>) nucleoFamiliarRepository.findAll();

		return familiaList;
	}
	
	@Override
	public NucleoFamiliar findById(Integer idPersona) {

		/**
		 * consulta las personas del nucleo por id
		 */
		NucleoFamiliar familia = nucleoFamiliarRepository.getOne(idPersona);

		return familia;
	}
	
	public List<NucleoFamiliar> findByPersona(Integer idPersona) {

		/**
		 * consulta el listado de todos los familiares de una familia
		 */
		List<NucleoFamiliar> familiaList = (List<NucleoFamiliar>) nucleoFamiliarRepository.findByPersona(idPersona);

		return familiaList;
	}
	
	@Override
	@Transactional
	public NucleoFamiliar save(NucleoFamiliar nucleoFamiliar) {
		NucleoFamiliar nucleoFailiarNew = nucleoFamiliarRepository.save(nucleoFamiliar);
		return nucleoFailiarNew;
	}
	
	@Override
	public void delete(NucleoFamiliar nucleoFamiliar) {
		nucleoFamiliarRepository.delete(nucleoFamiliar);
	}

}
