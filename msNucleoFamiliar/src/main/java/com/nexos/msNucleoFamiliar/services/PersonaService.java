package com.nexos.msNucleoFamiliar.services;

import com.nexos.msNucleoFamiliar.entities.Persona;

public interface PersonaService {

	/**
	 * Consulta una persona por id
	 * 
	 * @param id
	 * @return
	 */
	public Persona findById(Integer id);

}
