package com.nexos.msNucleoFamiliar.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nexos.msNucleoFamiliar.entities.Persona;
import com.nexos.msNucleoFamiliar.repositories.PersonaRepository;
import com.nexos.msNucleoFamiliar.services.PersonaService;

@Component
public class PersonaServiceImpl implements PersonaService{
	
	@Autowired
	PersonaRepository personaRepository;
	
	@Override
	public Persona findById(Integer id) {

		/**
		 * consulta las personas por id
		 */
		Persona persona = (Persona) personaRepository.getOne(id);

		return persona;
	}
}
