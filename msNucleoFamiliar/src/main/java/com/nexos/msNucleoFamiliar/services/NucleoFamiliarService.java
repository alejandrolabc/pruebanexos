package com.nexos.msNucleoFamiliar.services;

import java.util.List;

import com.nexos.msNucleoFamiliar.entities.NucleoFamiliar;

/**
*
* @author luis_balaguera
*/
public interface NucleoFamiliarService {
	
	/**
	 * Consulta todo el listado de familiares
	 * 
	 * @return
	 */
	public List<NucleoFamiliar> findAll();
	
	/**
	 * Consulta una Persona del nucleo por id
	 * 
	 * @param id
	 * @return
	 */
	public NucleoFamiliar findById(Integer id);
	
	/**
	 * Consulta todo el listado de familiares por persona
	 * 
	 * @return
	 */
	public List<NucleoFamiliar> findByPersona(Integer idPersona);
	
	/**
	 * Guarda una Persona del nucleo en DB
	 * 
	 * @param nucleoFamiliar
	 */
	public NucleoFamiliar save(NucleoFamiliar nucleoFamiliar);
	
	/**
	 * Elimina una Persona del nucleo en DB
	 * 
	 * @param nucleoFamiliar
	 */
	public void delete(NucleoFamiliar nucleoFamiliar);

}
