package com.nexos.prueba_micro_servicios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaMicroServiciosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaMicroServiciosApplication.class, args);
	}

}
