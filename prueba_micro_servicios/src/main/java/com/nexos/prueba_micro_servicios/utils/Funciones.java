package com.nexos.prueba_micro_servicios.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
*
* @author luis_balaguera
*/
public class Funciones {
	
	/**
	 * Consume un servicio web RESTful
	 * 
	 * @param headerContentType
	 * @param serviceUrl
	 * @param method
	 * @param responseType
	 * @param httpEntittyValue
	 * @return
	 */
	public static <T> ResponseEntity<?> consumeRestService(MediaType headerContentType, String serviceUrl,
			HttpMethod method, Class<T> responseType, Object httpEntittyValue) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(headerContentType);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<?> httpEntity = new HttpEntity<>(httpEntittyValue, headers);
		return restTemplate.exchange(serviceUrl, method, httpEntity, responseType);
	}
}
