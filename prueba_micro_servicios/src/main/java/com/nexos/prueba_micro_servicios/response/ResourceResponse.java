package com.nexos.prueba_micro_servicios.response;

import java.util.List;

import com.nexos.prueba_micro_servicios.dto.FamiliaDto;

/**
*
* @author luis_balaguera
*/
public class ResourceResponse extends Response{
		
	private List<FamiliaDto> familias;
	
	
	public ResourceResponse(Integer code, String message, String resource, List<FamiliaDto> familias) {
		super(code, message, resource);
		this.familias = familias;
	}


	public List<FamiliaDto> getFamilias() {
		return familias;
	}

	public void setFamilias(List<FamiliaDto> familias) {
		this.familias = familias;
	}
	
	
}
