package com.nexos.prueba_micro_servicios.dto;

import java.util.List;

/**
*
* @author luis_balaguera
*/
public class FamiliaDto {
	
	private PersonaDto persona;
	
	private List<NucleoFamiliarDto> nucleoFamiliar;

	public FamiliaDto(PersonaDto persona, List<NucleoFamiliarDto> nucleoFamiliar) {
		super();
		this.persona = persona;
		this.nucleoFamiliar = nucleoFamiliar;
	}

	public PersonaDto getPersona() {
		return persona;
	}

	public void setPersona(PersonaDto persona) {
		this.persona = persona;
	}

	public List<NucleoFamiliarDto> getNucleoFamiliar() {
		return nucleoFamiliar;
	}

	public void setNucleoFamiliar(List<NucleoFamiliarDto> nucleoFamiliar) {
		this.nucleoFamiliar = nucleoFamiliar;
	}
	
}
