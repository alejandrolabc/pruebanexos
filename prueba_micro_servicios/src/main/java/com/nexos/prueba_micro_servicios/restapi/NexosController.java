/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.prueba_micro_servicios.restapi;

import com.nexos.prueba_micro_servicios.dto.FamiliaDto;
import com.nexos.prueba_micro_servicios.dto.NucleoFamiliarDto;
import com.nexos.prueba_micro_servicios.dto.PersonaDto;
import com.nexos.prueba_micro_servicios.response.ResourceResponse;
import com.nexos.prueba_micro_servicios.utils.Funciones;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author luis_balaguera
 */
@RestController
public class NexosController {

	/**
	 * variable de log4j para logs de la aplicación
	 */
	private static final Logger logger = LogManager.getLogger(NexosController.class);

	private static String URL_BASE = "http://localhost:";
	private static String port_person = "8081";
	private static String port_nucleo = "8090";
	private static String div = "/";
	
	/**
     * consultarServicios.
     *
     * @param path este parametro contiene la ruta del path que consultara
     * el servicio
     * @return del archivo JSONObject que responde el servicio restful
     * @throws ParseException Signals that an I/O exception has occurred.
     */
	public JSONObject consultarServicios(String path, String port) throws ParseException {
		// Declarar un objeto que recibira la respuesta del servicio restful
		// del microservicio correspondiente al path que recibe
		ResponseEntity<?> infoService = null;
		// Declarar la url con el path del servicio
		String url = URL_BASE + port + div + path;
		// Usar el metodo consumeRestService de la clase Funciones para consultar
		// el servicio
		infoService = Funciones.consumeRestService(MediaType.APPLICATION_JSON, url, HttpMethod.GET, String.class, null);
		// Preparar un parseador de Json para la respuesta del servicio
		JSONParser parser = new JSONParser();
		// Parsear la respuesta en un JSONObject
		JSONObject obj = (JSONObject) parser.parse(infoService.getBody().toString());
		
		return obj;
	}

	@RequestMapping(value = "/Nexos/personas", method = RequestMethod.GET)
	public ResourceResponse getPersonas() {
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			// Llamar la funcion que consulta un servicio para el micro servicio msPersona
			JSONObject objPersona = consultarServicios("personas", port_person);
			// Extraer el codigo de la respuesta
			Long codigo = (Long) objPersona.get("code");
			// Validar si el servicio respondio correctamente
			if (codigo.equals(new Long("200"))) {
				// Peparar la lista de familias a retornar
				List<FamiliaDto> familias = new ArrayList<FamiliaDto>();
				JSONArray array = (JSONArray) objPersona.get("personas");
				for (int i = 0; i < array.size(); i ++) {
					// Capturara el json que tiene el array en su poición i
					JSONObject per = (JSONObject) array.get(i);
					// Extraer los valores del json por sus llaves
					Long idPersona = (Long) per.get("id");
					String nombrePersona = (String) per.get("nombre");
					String apellidoPersona = (String) per.get("apellido");
					String documentoPersona = (String) per.get("documento");
					String celularPersona = (String) per.get("celular");
					// Crear el objeto personaDto con su constructor
					PersonaDto personaDto = new PersonaDto(idPersona.intValue(), nombrePersona, apellidoPersona,
							documentoPersona, celularPersona);
					// Llamar la funcion que consulta un servicio para el micro servicio msNucleoFamiliar
					JSONObject objNucleo = consultarServicios("persona/"+idPersona.intValue()+"/nucleoFamiliar", port_nucleo);
					// Extraer el codigo de la respuesta
					Long codigoNucleo = (Long) objNucleo.get("code");
					if(codigoNucleo.equals(new Long("200"))) {
						// Capturar todos los familiares del servicio en una lista
						List<NucleoFamiliarDto> nucleoFamiliar = new ArrayList<NucleoFamiliarDto>();
						JSONArray arrayNucleo = (JSONArray) objNucleo.get("nucleoFamiliar");
						for (int j = 0; j < arrayNucleo.size(); j ++) {
							JSONObject nuc = (JSONObject) arrayNucleo.get(j);
							Long idFamiliar = (Long) nuc.get("id");
							String nombreFamiliar = (String) nuc.get("nombre");
							String apellidoFamiliar = (String) nuc.get("apellido");
							String documentoFamiliar = (String) nuc.get("documento");
							String celularFamiliar = (String) nuc.get("contacto");
							String parentescoFamiliar = (String) nuc.get("parentesco");
							NucleoFamiliarDto nfDto = new NucleoFamiliarDto(idFamiliar.intValue(), nombreFamiliar, apellidoFamiliar,
									documentoFamiliar, celularFamiliar, parentescoFamiliar);
							nucleoFamiliar.add(nfDto);
						}
						familias.add(new FamiliaDto(personaDto, nucleoFamiliar));
					} else {
						logger.info("Error servicio msNucleoFamiliar");
						resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
						resourceResponse.setMessage((String) objNucleo.get("message"));
					}
				}
				resourceResponse.setFamilias(familias);
				resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
				resourceResponse.setMessage("OK");
				logger.info("Correcto");
			} else {
				logger.info("Error servicio msPersona");
				resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
				resourceResponse.setMessage((String) objPersona.get("message"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
		}
		return resourceResponse;
	}

	@RequestMapping(value = "/Nexos/persona/{idPersona}", method = RequestMethod.GET)
	public ResourceResponse getPersona(@PathVariable("idPersona") Integer idPersona) {
		ResourceResponse resourceResponse = new ResourceResponse(null, null, null, null);
		try {
			// Llamar la funcion que consulta un servicio para el micro servicio msPersona
			JSONObject objPersona = consultarServicios("personas/"+idPersona, port_person);
			// Extraer el codigo de la respuesta
			Long codigo = (Long) objPersona.get("code");
			// Validar si el servicio respondio correctamente
			if (codigo.equals(new Long("200"))) {
				// Peparar la lista de familias a retornar
				List<FamiliaDto> familias = new ArrayList<FamiliaDto>();
				JSONArray array = (JSONArray) objPersona.get("personas");
				for (int i = 0; i < array.size(); i ++) {
					// Capturara el json que tiene el array en su poición i
					JSONObject per = (JSONObject) array.get(i);
					// Extraer los valores del json por sus llaves
					Long idPer = (Long) per.get("id");
					String nombrePersona = (String) per.get("nombre");
					String apellidoPersona = (String) per.get("apellido");
					String documentoPersona = (String) per.get("documento");
					String celularPersona = (String) per.get("celular");
					// Crear el objeto personaDto con su constructor
					PersonaDto personaDto = new PersonaDto(idPer.intValue(), nombrePersona, apellidoPersona,
							documentoPersona, celularPersona);
					// Llamar la funcion que consulta un servicio para el micro servicio msNucleoFamiliar
					JSONObject objNucleo = consultarServicios("persona/"+idPersona.intValue()+"/nucleoFamiliar", port_nucleo);
					// Extraer el codigo de la respuesta
					Long codigoNucleo = (Long) objNucleo.get("code");
					if(codigoNucleo.equals(new Long("200"))) {
						// Capturar todos los familiares del servicio en una lista
						List<NucleoFamiliarDto> nucleoFamiliar = new ArrayList<NucleoFamiliarDto>();
						JSONArray arrayNucleo = (JSONArray) objNucleo.get("nucleoFamiliar");
						for (int j = 0; j < arrayNucleo.size(); j ++) {
							JSONObject nuc = (JSONObject) arrayNucleo.get(j);
							Long idFamiliar = (Long) nuc.get("id");
							String nombreFamiliar = (String) nuc.get("nombre");
							String apellidoFamiliar = (String) nuc.get("apellido");
							String documentoFamiliar = (String) nuc.get("documento");
							String celularFamiliar = (String) nuc.get("contacto");
							String parentescoFamiliar = (String) nuc.get("parentesco");
							NucleoFamiliarDto nfDto = new NucleoFamiliarDto(idFamiliar.intValue(), nombreFamiliar, apellidoFamiliar,
									documentoFamiliar, celularFamiliar, parentescoFamiliar);
							nucleoFamiliar.add(nfDto);
						}
						familias.add(new FamiliaDto(personaDto, nucleoFamiliar));
					} else {
						logger.info("Error servicio msNucleoFamiliar");
						resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
						resourceResponse.setMessage((String) objNucleo.get("message"));
					}
				}
				resourceResponse.setFamilias(familias);
				resourceResponse.setCode(org.apache.http.HttpStatus.SC_OK);
				resourceResponse.setMessage("OK");
				logger.info("Correcto");
			} else {
				logger.info("Error servicio msPersona");
				resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
				resourceResponse.setMessage((String) objPersona.get("message"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error");
			logger.error(e.getMessage());
			resourceResponse.setCode(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
			resourceResponse.setMessage(e.getMessage());
		}
		return resourceResponse;
	}
}
