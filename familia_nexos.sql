-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.20 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para familia_nexos
CREATE DATABASE IF NOT EXISTS `familia_nexos` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `familia_nexos`;

-- Volcando estructura para tabla familia_nexos.audit_servicios
CREATE TABLE IF NOT EXISTS `audit_servicios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `servicio` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `path` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `metodo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `codigo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `descripcion` longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla familia_nexos.audit_servicios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `audit_servicios` DISABLE KEYS */;
INSERT INTO `audit_servicios` (`id`, `servicio`, `path`, `metodo`, `codigo`, `descripcion`, `fecha`) VALUES
	(1, 'msNucleoFamiliar', '/nucleoFamiliar', 'GET', '0', 'entro al servicio', '2020-05-29 07:12:01'),
	(2, 'msNucleoFamiliar', '/nucleoFamiliar', 'GET', '200', 'OK', '2020-05-29 07:12:03'),
	(3, 'msNucleoFamiliar', '/persona/1/nucleoFamiliar', 'GET', '0', 'entro al servicio', '2020-05-29 07:12:47'),
	(4, 'msNucleoFamiliar', '/persona/1/nucleoFamiliar', 'GET', '200', 'OK', '2020-05-29 07:12:47'),
	(5, 'msNucleoFamiliar', '/nucleoFamiliar', 'POST', '200', 'OK', '2020-05-29 07:13:38'),
	(6, 'msNucleoFamiliar', '/nucleoFamiliar/7', 'PUT', '0', 'com.nexos.msNucleoFamiliar.entities.NucleoFamiliar@60afe37b', '2020-05-29 07:14:58'),
	(7, 'msNucleoFamiliar', '/nucleoFamiliar/7', 'PUT', '200', 'OK', '2020-05-29 07:14:59'),
	(8, 'msNucleoFamiliar', '/nucleoFamiliar/7', 'DELETE', '0', 'entro al servicio', '2020-05-29 07:17:31'),
	(9, 'msNucleoFamiliar', '/nucleoFamiliar/7', 'DELETE', '500', 'could not execute statement; SQL [n/a]; constraint [null]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement', '2020-05-29 07:17:32');
/*!40000 ALTER TABLE `audit_servicios` ENABLE KEYS */;

-- Volcando estructura para tabla familia_nexos.nucleo_familiar
CREATE TABLE IF NOT EXISTS `nucleo_familiar` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `apellido` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `documento` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `contacto` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `id_persona` int NOT NULL DEFAULT '0',
  `id_parentesco` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_nucleo_familiar_persona` (`id_persona`),
  KEY `fk_nucleo_familiar_parentesco` (`id_parentesco`),
  CONSTRAINT `fk_nucleo_familiar_parentesco` FOREIGN KEY (`id_parentesco`) REFERENCES `parentesco` (`id`),
  CONSTRAINT `fk_nucleo_familiar_persona` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla familia_nexos.nucleo_familiar: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `nucleo_familiar` DISABLE KEYS */;
INSERT INTO `nucleo_familiar` (`id`, `nombre`, `apellido`, `documento`, `contacto`, `id_persona`, `id_parentesco`) VALUES
	(1, 'Luis', 'Balaguera', '19192812', '312637383', 1, 1),
	(2, 'Yenny', 'Castañeda', '1019239393', '3123039002', 1, 2),
	(3, 'Daniel', 'Balaguera', '1019239393', '3123039002', 1, 3),
	(5, 'Cristina', 'Teran', '2910292', '320839392', 2, 4),
	(6, 'Pedro Manuel', 'Narvaez Teran', '22222', '322125951', 2, 3),
	(7, 'Manuel', 'Teran', '22222', '322125951', 2, 1);
/*!40000 ALTER TABLE `nucleo_familiar` ENABLE KEYS */;

-- Volcando estructura para tabla familia_nexos.parentesco
CREATE TABLE IF NOT EXISTS `parentesco` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `PK_parentesco` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla familia_nexos.parentesco: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `parentesco` DISABLE KEYS */;
INSERT INTO `parentesco` (`id`, `tipo`) VALUES
	(1, 'Padre'),
	(2, 'Madre'),
	(3, 'Hermano'),
	(4, 'Hermana'),
	(5, 'Abuelo'),
	(6, 'Abuela');
/*!40000 ALTER TABLE `parentesco` ENABLE KEYS */;

-- Volcando estructura para tabla familia_nexos.persona
CREATE TABLE IF NOT EXISTS `persona` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `apellido` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `documento` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `celular` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `PK_persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla familia_nexos.persona: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` (`id`, `nombre`, `apellido`, `documento`, `celular`) VALUES
	(1, 'Alejandro', 'Balaguera', '1022397779', '3105713960'),
	(2, 'Johana', 'Narvaez', '1019239393', '3123039002');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
